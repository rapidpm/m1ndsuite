package org.rapidpm.mindsuite.persistence.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 * M1ndSuite - www.rapidpm.org
 * User: Marco Ebbinghaus
 * Date: 11.12.13
 * Time: 11:47
 */
@Deprecated
public class DAO {

    @Autowired
    protected JdbcTemplate jdbcTemplate;

}
