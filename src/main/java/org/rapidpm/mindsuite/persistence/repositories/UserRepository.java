package org.rapidpm.mindsuite.persistence.repositories;

import org.rapidpm.mindsuite.persistence.entities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    public List<User> findByLastName(String lastName);
}