package org.rapidpm.mindsuite.persistence.repositories;

import org.rapidpm.mindsuite.persistence.entities.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * M1ndSuite - www.rapidpm.org
 * User: Marco Ebbinghaus
 * Date: 11.12.13
 * Time: 10:11
 */
@Deprecated
public class UserDAO extends DAO {

    public List<User> getAllUsers(){
        final List<User> users = jdbcTemplate.query("select * from test",
                new RowMapper<User>() {
                    @Override
                    public User mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new User(rs.getString("first_name"), rs.getString("last_name"));
                    }
                });
        return users;
    }
}
