package org.rapidpm.mindsuite.controllers;

import org.rapidpm.mindsuite.persistence.entities.User;
import org.rapidpm.mindsuite.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class RootController {

//    @Autowired
//    private UserDAO userDAO;
//    @Autowired
//    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @RequestMapping("/")
    public String greeting(final Model model) {
        final List<User> userList = userService.findAll();
        model.addAttribute("users",userList);
        return "root";
    }

}