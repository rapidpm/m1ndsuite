package org.rapidpm.mindsuite;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan
@EnableAutoConfiguration
public class MindsuiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(MindsuiteApplication.class, args);
    }

}