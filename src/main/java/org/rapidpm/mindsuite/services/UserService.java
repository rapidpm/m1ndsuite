package org.rapidpm.mindsuite.services;

import org.rapidpm.mindsuite.persistence.entities.User;
import org.rapidpm.mindsuite.persistence.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * M1ndSuite - www.rapidpm.org
 * User: Marco Ebbinghaus
 * Date: 18.12.13
 * Time: 11:04
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> findAll() {
        final List<User> users = new ArrayList<User>();
        for (final User user : userRepository.findAll()) {
            users.add(user);
        }
        return users;
    }

    public void save(final User user){
        userRepository.save(user);
    }

}
